<h1 align=center>Minecraft Telegram Bot</h1>
<p align=center>Um bot para o Telegram que faz ponte com o ngrok e envia o IP em um chat.</p>

## Como funciona?
Você abre seu servidor na porta 25565, e então o bot fará o resto, você irá conseguir jogar com amigos sem precisar abrir as portas do seu roteador.

## Como instalar e configurar

Em primeiro lugar, clone o repositório:
```
git clone https://github.com/RedsonBr140/telegram-bot.git && cd telegram-bot
```

Agora, você deve saber que este bot foi criado para ser rodado em um sistema GNU/Linux, e você precisa instalar as dependencias dele:
```bash
pip install -r requirements.txt
```
Outra dependencia necessária para o funcionamento do bot é o `jq`, que deve ser instalado pelo gerenciador de pacotes do seu sistema.

Depois disso, rode o bot.py para baixar o ngrok localmente:
```
python3 bot.py
```
**Configure o ngrok como pedido no Script se for sua primeira vez executando.**

Agora, você deve copiar o .env.example e fazer modifica-lo:
```bash
cp .env.example .env
```

No arquivo .env que você copiou, temos o seguinte conteúdo:
```env
TOKEN="TOKEN HERE"
CHATID="ChatID Here"
```
O Token é o Token do Bot, que você pode conseguir com o [BotFather](https://t.me/BotFather), já o CHATID é o id do chat que será enviada a mensagem quando o servidor estiver online, você pode obte-lo acessando
`api.telegram.org/bot<TokenDoBot>/getUpdates`. Com tudo configurado você pode rodar o bot usando:

```python
python3 bot.py
```

## Equipe

Esse projeto é mantido pelas pessoas abaixo:

| [![RedsonBr140](https://github.com/RedsonBr140.png?size=100)](https://github.com/RedsonBr140) | [![S0ra](https://github.com/S0raWasTaken.png?size=100)](https://github.com/S0raWasTaken) | [![Franklin Souza](https://github.com/ffraanks.png?size=100)](https://github.com/ffraanks)
| ----------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| [RedsonBr140](https://github.com/RedsonBr140) | [S0raWasTaken](https://github.com/S0raWasTaken) | [Franklin Souza](https://github.com/ffraanks)

Obrigado a cada um de vocês, por fazerem parte desse projeto incrível.

---
:heart: Keep It Simple, Stupid.


