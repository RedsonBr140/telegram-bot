import sys
import signal
import telebot # Importa o telebot.
from os import system,getenv # Importa a função system do módulo os.
from os.path import exists # Importa a função exists do módulo os.path
from dotenv import load_dotenv # Importa a função load_dontenv do modulo dotenv.
from wget import download
from time import sleep
from subprocess import check_output

if exists("ngrok") == False:
    print(f"Ngrok não existe. Efetuarei o download!")
    file_name = download("https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip")
    print(f"\nDescompactando o {file_name}")
    system(f"unzip {file_name}")
    system(f"rm {file_name}")
    print("\nAgora o binário do ngrok está corretamente 'instalado', você deve criar uma conta em https://dashboard.ngrok.com e efetuar o passo 2, após isso, poderá rodar o bot corretamente.\n")
    exit(1)
    
load_dotenv()  # Pega as variáveis de ambiente do dotenv.

system("./ngrok tcp 25565 > /dev/null &") # Inicia o serviço do Ngrok
sleep(1) # Aguarda um segundo para o Ngrok subir

webhook_url = check_output(["bash","-c","curl -Ss http://localhost:4040/api/tunnels | jq \".tunnels[0].public_url\""]).decode("utf-8").split("//")[1].split("\"")[0]
#print(webhook_url)

# Vars
TOKEN = getenv('TOKEN')
chatid = getenv('CHATID')
tb = telebot.TeleBot(TOKEN)	# Cria um novo objeto.
user = tb.get_me() # Pega o username
tb.send_message(chatid, f'Server ON:\n\nIP: {webhook_url}\nVersão: 1.12.2\nGame: Minecraft\nHost OS: GNU/Linux') # Envia uma mensagem
print('Bot rodando...')
tb.polling()

def sig_handler(s, f):
    system('clear')
    tb.send_message(chatid, f'Server OFF:\n\nIP: {webhook_url}\nVersão: 1.12.2\nGame: Minecraft\nHost OS: GNU/Linux')
    system("killall ngrok")
    sys.exit(0)

signal.signal(signal.SIGINT, sig_handler)
signal.pause()

